document.addEventListener("DOMContentLoaded", function () {
    const signUpBtn = document.getElementById("Sign-Up");
    const signInBtn = document.getElementById("Sign-In");

    const nameInput = document.getElementById("name");
    const emailInput = document.getElementById("email");
    const passwordInput = document.getElementById("password");

    const inputFields = document.querySelectorAll('.input-field');
    const h1 = document.querySelector('.form-box h1');

    signUpBtn.onclick = function () {
        h1.innerHTML = 'Sign Up';
        signUpBtn.classList.remove("disable");
        signInBtn.classList.add("disable");
        nameInput.parentElement.style.display = 'flex'; // Show the name input when signing up
    };

    signInBtn.onclick = function () {
        nameInput.parentElement.style.display = 'none';
        h1.innerHTML = 'Sign In'; // Make sure to update the text consistently
        signUpBtn.classList.add("disable");
        signInBtn.classList.remove("disable");
    };
});





















/*const signUpBtn = document.getElementById("Sign-Up");
const signInBtn = document.getElementById("Sign-In");

const nameInput = document.getElementById("name");
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("password");

const inputFields = document.querySelectorAll('.input-field');
const h1 = document.querySelector('.form-box h1');

signUpBtn.onclick = function () {
    
    nameInput.parentElement.style.maxHeight = "60px";
    h1.innerHTML='sign Up'
    signUpBtn.classList.remove("disable")
    signInBtn.classList.add("disable")
 
   
  
};
signInBtn.onclick = function () {
    nameInput.parentElement.style.maxHeight = "0";
    h1.innerHTML = 'Sign In'; // Make sure to update the text consistently
    signUpBtn.classList.add("disable");
    signInBtn.classList.remove("disable");
};

*/








